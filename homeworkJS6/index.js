    function CreateNewUser() {
        this.firstName = prompt("Enter Your Name");
        this.lastName = prompt("Enter Your last Name");
        //        Object.defineProperties(this, {
        //            "firstName": {
        //                configurable: false
        //                , writable: false
        //            }
        //            , "lastName": {
        //                configurable: false
        //                , writable: false
        //            }
        //        });
        Object.defineProperty(this, "getLogin", {
            get: function () {
                return this.firstName[0].toLowerCase() + this.lastName.toLowerCase();
            }
        });
    }
    let user = (new CreateNewUser());
    console.log(user);
    console.log(user.getLogin);
    delete user.firstName;
    user.firstName = "Kostya";
    console.log(user.firstName);
    delete user.lastName;
    user.lastName = "Vova";
    console.log(user.lastName);