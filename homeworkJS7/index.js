const users = [{
        name: "Ivan"
        , surname: "Ivanov"
        , gender: "male"
        , age: 30
    }
    , {
        name: "Anna"
        , surname: "Ivanova"
        , gender: "female"
        , age: 22
    }
    , {
        name: "Andrey"
        , surname: "Ivanov"
        , gender: "uknown"
        , age: 35
    }];
const admins = [{
        name: "Ivan"
        , surname: "Duran"
        , gender: "male"
        , age: 30
    }
    , {
        name: "Vova"
        , gender: "female"
        , age: 22
    }
    , {
        name: "Petro"
        , surname: "Ivanov"
        , gender: "male"
        , age: 22
    }];

function excludeBy(arrOne, arrTwo, property) {
    return arrOne.filter(function (obj) {
        let check = true;
        for (let i = 0; i < arrTwo.length; i++) {
            if (obj[property] == arrTwo[i][property]) {
                check = false;
            }
        }
        if (check) return obj;
    });
}
console.log(excludeBy(users, admins, "name"));