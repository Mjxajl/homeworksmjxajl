function cloneObj(origin) {
    let clone = {};
    for (let key in origin) {
        if (typeof origin[key] === "object") cloneObj(origin[key]);
        clone[key] = origin[key];
    }
    return clone;
}
let obj = {
    height: 100
    , width: 200
    , depth: 300
    , object: {
        h: 100
        , w: 200
        , s: 300
        , test: {
            a: 100
        }
    }
    , array: ["arr1", "arr2", "arr3"]
};
let testClone = cloneObj(obj);
console.log(testClone);
testClone.object.h++;
console.log(testClone.object.h);
console.log(obj.object.h);