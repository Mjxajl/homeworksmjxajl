let msec = 0
    , sec = 0
    , min = 0
    , click = true
    , timer, timeStart;

function start() {
    if (click) {
        click = false;
        timeStart = new Date().getTime() - (msec + (sec + min * 60) * 1000);
        document.getElementById("start").textContent = "pause";
        timer = setInterval(function () {
            document.getElementById("msec").textContent = msec += 4;
            if (msec > 1000) {
                document.getElementById("sec").textContent = ++sec;
                msec -= 1000;
            };
            if (sec > 59) {
                document.getElementById("sec").textContent = sec = 0;
                document.getElementById("min").textContent = ++min;
            };
            msec += new Date().getTime() - (timeStart + msec + (sec + min * 60) * 1000 + min);
        }, 1);
    }
    else {
        pause();
    };
}

function pause() {
    clearInterval(timer);
    click = true;
    document.getElementById("start").textContent = "start";
};

function clearTime() {
    pause();
    document.getElementById("msec").textContent = document.getElementById("sec").textContent = document.getElementById("min").textContent = msec = sec = min = 0;
};
