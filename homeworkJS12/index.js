let radius;

function addInput() {
    document.body.innerHTML = `<label for="inputradius">Введите радиус круга</label> <input type="number" id="inputradius"><button onclick=  hundredCircles()>Нарисовать</button>`
}

function hundredCircles() {
    radius = document.getElementById('inputradius').value;
    document.body.setAttribute('onclick', `if(event.target != this) {event.target.style.opacity=0}`);
    document.body.setAttribute('style', `white-space: nowrap`)
    document.body.innerHTML = "";
    for (let i = 1; i < 101; i++) {
        document.body.innerHTML += `<div style = width:${radius}px;height:${radius}px;background-color:rgb(${Math.round(Math.random() * (256 - 0))},${Math.round(Math.random() * (256 - 0))},${Math.round(Math.random() * (256 - 0))});border-radius:50%;display:inline-block></div>

`;
        if (i % 10 == 0) {
            document.body.innerHTML += `<br></br>`;
        }
    }
}
document.getElementById("button").setAttribute('onclick', `addInput()`);